package com.jiraupload.views;

import com.jiraupload.panels.SettingsPanel;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.logging.Logger;

/**
 * Created by robert.j.ssemmanda on 09/09/2017.
 * We use this class to manage the view to be displayed.
 * In this case it the main view.... the default view
 */

@SpringView(name = MainView.VIEW_NAME)
@VaadinSessionScope
public class MainView extends VerticalLayout implements View{
    public static final String VIEW_NAME = "";
    private static final Logger logger = Logger.getLogger(MainView.class.getName());

    //our settings layout and panel
    @Autowired
    SettingsPanel settingsPanel;

    //we use this method to create and setup our view.
    @PostConstruct
    private void init(){
        //setSizeFull();
        settingsPanel.setLoginButtonListener(clickEvent -> {
            logger.info("He did click the login button");
        });
        addComponent(settingsPanel);
        setComponentAlignment(settingsPanel, Alignment.TOP_CENTER);
    }

}
