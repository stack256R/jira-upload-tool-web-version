package com.jiraupload.factory;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by robert.j.ssemmanda on 20/07/2017.
 */

/*This class will enable us to create various UI elements.
This is a simple implementation of a vactory design pattern. The advantage with this,
is that we do not have to rewrite the UI components creation code everywhere in the project*/

public class UIElementsFactory {

    public static Button createButton(String caption, Button.ClickListener clickListener, String style){
        Button button = new Button(caption);
        if(style == null || style.trim().isEmpty()){
            button.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
        }else{
            button.addStyleName(style);
        }
        if(clickListener !=null){
            button.addClickListener(clickListener);
        }
        return button;
    }

    public static TextField createTextField(String caption, TextField.ValueChangeListener valueChangeListener, String style, int maxLength, boolean isDouble ){
        //TextField textField = (isDouble)? new DoubleTextField(): new TextField();
        TextField textField = new TextField();
        if(maxLength > 0){textField.setMaxLength(maxLength);}
        if(valueChangeListener != null){
            textField.addValueChangeListener(valueChangeListener);
        }

        if(style == null || style.trim().isEmpty()){
            textField.addStyleName("v-textfield-wb");
        }else{
            textField.addStyleName(style);
        }

        if(!(caption == null || caption.trim().isEmpty())){
            textField.setCaption(caption);
        }

        textField.setWidth(80.0f, Sizeable.Unit.PERCENTAGE);

        return textField;
    }

    public static Label createLabel(String text, String style){
        Label label = new Label(text);
        if(style != null){label.setStyleName(style);}
        else{
            label.setStyleName(ValoTheme.LABEL_LIGHT);
        }
        return label;
    }

    public static TextArea createTextArea(String caption, TextArea.ValueChangeListener valueChangeListener, String style ){
        TextArea textArea = new TextArea();
        if(valueChangeListener != null){
            textArea.addValueChangeListener(valueChangeListener);
        }

        if(style == null || style.trim().isEmpty()){
            textArea.addStyleName(ValoTheme.TEXTAREA_BORDERLESS);
        }else{
            textArea.addStyleName(style);
        }
        return textArea;
    }

    public static FormLayout createFormLayout(String style){
        FormLayout formLayout =  new FormLayout();
        if(style == null || style.isEmpty()){
            //formLayout.setStyleName(style);
        }else {
            //formLayout.setStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        }
        return formLayout;
    }

    public static HorizontalLayout createHorizonatlLayout(String style){
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        if(!(style == null || style.trim().isEmpty())){
            horizontalLayout.addStyleName(style);
        }
        return horizontalLayout;
    }

    public static VerticalLayout creatVerticalLayout(String style){
        VerticalLayout verticalLayout = new VerticalLayout();
        if(!(style == null || style.trim().isEmpty())){
            verticalLayout.addStyleName(style);
        }
        return verticalLayout;
    }

    public static Panel createPanel(String style){
        Panel panel = new Panel();
        if(style == null || style.trim().length() == 0){
            panel.addStyleName(ValoTheme.PANEL_WELL);
        }else{
            panel.addStyleName(style);
        }
        return panel;
    }

    public static TabSheet createTabSheet(String style){
        TabSheet tabSheet = new TabSheet();
        tabSheet.addStyleName((StringUtils.isNotBlank(style))?ValoTheme.TABSHEET_EQUAL_WIDTH_TABS:style);
        return tabSheet;
    }
    /*public static ComboBox createComboBox(String caption){

    }*/

}
