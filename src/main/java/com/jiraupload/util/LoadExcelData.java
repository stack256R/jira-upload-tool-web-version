package com.jiraupload.util;

import com.jiraupload.model.Fields;
import com.jiraupload.model.Steps;
import com.jiraupload.model.TestCase;
import com.jiraupload.services.UploadService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by robert.j.ssemmanda on 05/09/2017.
 * <p>
 * We use this class for all our excel functions and activities
 */
public class LoadExcelData {

    private static final Logger logger = Logger.getLogger(LoadExcelData.class.getName());

    public static List<TestCase> loadIssuesFromExcelFile(
            InputStream inputStream, Discriminator discriminator,
            Map<Fields, String> columns, String filterLists,
            boolean uploadAll, int startRow, String mainColumn, String summaryCol) {
        Workbook workbook;
        int mainColumnInt = CellReference.convertColStringToIndex(mainColumn);
        int summaryColInt = CellReference.convertColStringToIndex(summaryCol);
        List<TestCase> toReturn = null;
        try {

            workbook = WorkbookFactory.create(inputStream);
            toReturn = readExcelFile(workbook, columns, filterLists, uploadAll, startRow, mainColumnInt, summaryColInt, discriminator);
        } catch (InvalidFormatException | IOException e) {
            logger.info(e.getLocalizedMessage());
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            logger.info(e.getLocalizedMessage());
        }
        return toReturn;
    }


    private static List<TestCase> readExcelFile(
            Workbook workbook, Map<Fields, String> columns,
            String filterLists, boolean uploadAll,
            int startRow, int mainColumn, int summaryCol, Discriminator discriminator) {
        List<TestCase> testCaseList = new ArrayList<>();
        //FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
        List<String> uploadLists = uploadAll ? null : Arrays.asList(StringUtils.split(filterLists, ","));

        List<TestCase> temp;
        if (uploadAll) {
            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                temp = getAllTestCasesForSheet(workbook.getSheetAt(i), discriminator, columns, startRow, mainColumn, summaryCol);
                testCaseList.addAll(temp);
            }
        } else if (!uploadLists.isEmpty()) {
            Sheet localSheet;
            for (String lsheet : uploadLists) {
                localSheet = workbook.getSheet(lsheet);
                if (localSheet != null) {
                    temp = getAllTestCasesForSheet(localSheet, discriminator, columns, startRow, mainColumn, summaryCol);
                    testCaseList.addAll(temp);
                }
            }
        }
        return testCaseList;
    }

    private static TestCase testCaseFromRow(Row row, TestCase testCase, int summaryCol, Map<Fields, String> columns) {
        if (testCase == null && row.getCell(summaryCol) != null) {
            testCase = new TestCase();
            testCase.setDefaultName(row.getCell(summaryCol).getStringCellValue());
        }
        int col;
        Cell cell;
        Steps steps = null;
        if (testCase != null) { //valid testcase scenario
            for (Map.Entry<Fields, String> entry : columns.entrySet()) { //loop through each column and get the value there
                col = CellReference.convertColStringToIndex(entry.getValue());
                cell = row.getCell(col);
                if (cell != null) {
                    if (entry.getKey().getName().equals(UploadService.TEST_STEP_NAME)
                            || entry.getKey().getName().equals(UploadService.TEST_STEP_DATA)
                            || entry.getKey().getName().equals(UploadService.TEST_STEP_RESULTS)) {
                        if (steps == null) {
                            steps = new Steps();
                        }
                        switch (entry.getKey().getName()) {
                            case UploadService.TEST_STEP_NAME:
                                steps.setStep(cell.getStringCellValue());
                                break;
                            case UploadService.TEST_STEP_DATA:
                                steps.setTestData(cell.getStringCellValue());
                                break;
                            case UploadService.TEST_STEP_RESULTS:
                                steps.setResult(cell.getStringCellValue());
                                break;
                        }
                    } else {
                        //we only add no step data to te issue data
                        testCase.addIssueData(entry.getValue(), cell.getStringCellValue());
                    }
                }

            }
        }
        if (steps != null) {
            testCase.addSteps(steps);
        }
        return testCase;
    }


    private static List<TestCase> getAllTestCasesForSheet(
            Sheet sheet, Discriminator discriminator,
            Map<Fields, String> columns, int startRow,
            int mainColumn, int summaryCol) {
        List<TestCase> testCaseList = new ArrayList<>();
        TestCase testCase = null;
        Row row;
//        Cell cell = null;

        for (int i = startRow - 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            row = sheet.getRow(i);
            switch (discriminator) {
                case BY_TESTCASE_NAME_CHANGE:
                case BY_ID_CHANGE: {
                    //check whether the main column has changed or not
                    if (row.getCell(mainColumn) != null //if its blank we assume its the same testcase
                            && testCase != null
                            && !StringUtils.equalsIgnoreCase(row.getCell(mainColumn).getStringCellValue(), (String) testCase.getFirstValueForColumn(CellReference.convertNumToColString(mainColumn)))) {
                        //different test case now
                        addTestCaseToList(testCaseList, testCase);
                        testCase = null;
                    }
                }
                break;
                case BY_EMPTY_ROW: {
                    if (checkIfRowIsEmpty(row)) {
                        addTestCaseToList(testCaseList, testCase);
                        testCase = null;
                    }
                }
                break;
            }
            testCase = testCaseFromRow(row, testCase, summaryCol, columns);
        }
        if (testCase != null) {
            addTestCaseToList(testCaseList, testCase);
        }
        return testCaseList;
    }

    private static void addTestCaseToList(List<TestCase> testCaseList, TestCase testCase) {
        if (testCase != null) {
            if (testCaseList == null) {
                testCaseList = new ArrayList<>();
            }
            testCaseList.add(testCase);
        }
    }

    private static boolean checkIfRowIsEmpty(Row row) {
        if (row == null) {
            return true;
        }
        if (row.getLastCellNum() <= 0) {
            return true;
        }
        for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
            Cell cell = row.getCell(cellNum);
            if (cell != null && cell.getCellTypeEnum() != CellType.BLANK && StringUtils.isNotBlank(cell.toString())) {
                return false;
            }
        }
        return true;
    }

}
