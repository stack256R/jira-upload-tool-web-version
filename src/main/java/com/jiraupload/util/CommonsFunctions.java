package com.jiraupload.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


/**
 * Created by robert.j.ssemmanda on 31/08/2017.
 */
@Component
@Scope("singleton")
public class CommonsFunctions {
    private static final Logger logger = Logger.getLogger(CommonsFunctions.class.getName());
    private final static String sslUnsafeRenego = "sun.security.ssl.allowUnsafeRenegotiation";
    private final static String sslAllowedLegacy = "sun.security.ssl.allowLegacyHelloMessages";

    /*
    * We use this function to disable ssl authentications... as we are in an internal network
    * we do are not that concerned about this
    * */
    private static void setUpHttpsPermissions() {
        System.setProperty(sslUnsafeRenego, "true");
        System.setProperty(sslAllowedLegacy, "true");
    }

    //this method is used to extract and save a cookie from one header to another (set cookie)
    public static void retrieveAndSaveCookie(HttpHeaders responseHeader, HttpHeaders httpHeaders) {
        saveCookie(retrieveCookie(responseHeader), httpHeaders);
    }

    //we use this function to step and save a cookie to a provided httpheader
    private static void saveCookie(String cookie, HttpHeaders httpHeaders) {
        //extract the header for the cookie.
        if (org.apache.commons.lang3.StringUtils.isNotBlank(cookie)) { //we set or cookie
            httpHeaders.add(HttpHeaders.COOKIE, cookie);
        }
    }

    //this function extracts a cookie from a header
    private static String retrieveCookie(HttpHeaders httpHeaders) {
        String cookie = null;
        List<String> cookies = httpHeaders.get(HttpHeaders.SET_COOKIE);
        if (!(cookies == null || cookies.isEmpty())) {
            cookie = StringUtils.join(cookies, "; ");
        }
        return cookie;
    }


    //we use this method to convert a map into a json string
    public static String convertMapToJsonString(Map map) {
        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    //we use this method to check if a response status is ok or else display an error message
    public static boolean isResponseOk(ResponseEntity responseEntity) {
        if (responseEntity != null) {
            return responseEntity.getStatusCode().is2xxSuccessful();
        }
        logger.info("Response not ok ==> null ");
        return false;
    }

    //we use this method to perform any call that we have, we make it static so that it can be used asynch
    public static ResponseEntity performRestCall(String url,
                                                 String jsonData,
                                                 HttpMethod httpMethod,
                                                 HttpHeaders httpHeaders,
                                                 Class returnClass) throws RestClientException {
        setUpHttpsPermissions();
        ResponseEntity responseEntity;
        RestTemplate restTemplate = getRestTemplate();
        responseEntity = restTemplate.exchange(url,
                httpMethod,
                new HttpEntity<>(
                        StringUtils.isNotBlank(jsonData) ? jsonData : null,
                        httpHeaders),
                returnClass);
        return responseEntity;
    }

    //we use this method to return a resttemplate object with the acquired cookie to make the requests
    private static RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    public static Map<String, Object> createMapFromJsonString(String jsonData) {
        Map<String, Object> toReturn = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            toReturn = objectMapper.readValue(jsonData, new TypeReference<Map<String, Object>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return toReturn;
    }

    public static List createListFromJsonString(String jsonData) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(jsonData, TypeFactory.defaultInstance().constructCollectionType(List.class, Object.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isJsonValid(String json) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.readTree(json);
            return true;
        } catch (IOException e) {
            logger.info(e.getLocalizedMessage());
        }
        return false;
    }

    /*
    * We use this function to create a json string from our username and password
    * */
    public String createUserJsonMap(String username, char[] password) {
        String json = null;
        Map<String, Object> jsonData = new HashMap<>();
        jsonData.put("username", username);
        jsonData.put("password", password);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            json = objectMapper.writeValueAsString(jsonData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

   /* public String sanitizeRequestUrl(String urlFragment, String urlBase) {
        try {
            URL baseUrl = new URL(urlBase.replaceFirst("/$", ""));
            if (!StringUtils.endsWith(baseUrl.getFile(), "/rest")) {
                baseUrl = new URL(
                        baseUrl.getProtocol(),
                        baseUrl.getHost(),
                        baseUrl.getPort(),
                        baseUrl.getFile() + "/rest");
            }
            return sanitizeRequestUrl(urlFragment, baseUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String sanitizeRequestUrl(String urlFragment, URL origBaseUrl) {
        if (origBaseUrl.getProtocol().indexOf("https") > -1) {
            setUpHttpsPermissions();
        }
        return origBaseUrl.toString() + urlFragment;
    }*/

}
