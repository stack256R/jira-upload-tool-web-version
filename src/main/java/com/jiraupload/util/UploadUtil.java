package com.jiraupload.util;

import com.jiraupload.model.*;
import com.jiraupload.services.UploadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.jiraupload.util.CommonsFunctions.convertMapToJsonString;

/**
 * Created by robert.j.ssemmanda on 08/09/2017.
 * This class houses all the function we need while uploading
 * our test cases to the server
 */
public class UploadUtil {

    private static final Logger logger = Logger.getLogger(UploadUtil.class.getName());

    private static Object prepareDataForField(Fields fields, String data) {
        String toReturn = data;
        Object prepData = null;
        List arrays;
        Map<String, String> temp;
        Object matchingObject;
        if (fields.getSchema() != null && StringUtils.isNotBlank(data)) { //we need to know how to format this or else we default to string
            switch (fields.getSchema().getType()) {
                case "array": {
                    arrays = Arrays.asList(toReturn.split(","));
                    if (!fields.getName().equalsIgnoreCase("labels")) {
                        arrays.stream().filter(version -> StringUtils.isNotBlank((String) version))
                                .map(version -> {
                                    Map<String, Object> map = new HashMap();
                                    map.put("name", version);
                                    return map;
                                })
                                .collect(Collectors.toList());
                    }
                    if (!(fields.getAllowedValues() == null || fields.getAllowedValues().isEmpty())) {
                        List<Object> listMap = new ArrayList<>();
                        for (Object item : arrays) {
                            matchingObject = prepareDataFromAllowed(fields.getAllowedValues(), item);
                            if (matchingObject != null) {
                                listMap.add(matchingObject);
                            }
                        }
                        prepData = listMap;
                    } else {
                        prepData = arrays;
                    }
                }
                break;
                case "priority":
                case "user":
                case "project": {
                    temp = new HashMap<>();
                    temp.put("name", toReturn);
                    if (!(fields.getAllowedValues() == null || fields.getAllowedValues().isEmpty())) {
                        matchingObject = prepareDataFromAllowed(fields.getAllowedValues(), temp);
                    } else {
                        prepData = temp;
                    }
                }
                break;
                case "numberfield":
                case "NumberField": {
                    try {
                        prepData = NumberFormat.getInstance().parse(toReturn.trim());
                    } catch (ParseException e) {
                        logger.info(e.getLocalizedMessage());
                    }
                }
                break;
                default: { //all else will be treated as an ordinary string .. we check if they are json objects though
                    if (fields.getKey().equals("summary")) {
                        toReturn = StringUtils.replace(toReturn, "\n", " ");
                    } else if (!fields.getKey().equals("description")) {
                        prepData = checkJsonStringInput(toReturn.trim());
                    }
                    if (prepData == null) {
                        if (!(fields.getAllowedValues() == null || fields.getAllowedValues().isEmpty())) {
                            prepData = prepareDataFromAllowed(fields.getAllowedValues(), toReturn);
                        } else {
                            prepData = toReturn;
                        }
                    }
                }
                break;
            }
        }
        return prepData;
    }

    private static Object checkJsonStringInput(String input) {
        Object prepData = null;
        if (StringUtils.startsWith(input.trim(), "{")
                && StringUtils.endsWith(input.trim(), "}")) { //json object
            if (CommonsFunctions.isJsonValid(input.trim())) {
                prepData = CommonsFunctions.createMapFromJsonString(input.trim());
            }
        } else if (StringUtils.startsWith(input.trim(), "[")
                && StringUtils.endsWith(input.trim(), "]")) { //json array
            if (CommonsFunctions.isJsonValid(input.trim())) {
                prepData = CommonsFunctions.createListFromJsonString(input.trim());
            }
        } else { //just an ordinary String
            prepData = input;
        }
        return prepData;
    }

    private static boolean isStepField(Fields fields) {
        return fields.getName().equals(UploadService.TEST_STEP_NAME)
                || fields.getName().equals(UploadService.TEST_STEP_DATA)
                || fields.getName().equals(UploadService.TEST_STEP_RESULTS);
    }

    private static List<Map<String, String>> creatTestStepsBody(TestCase testCase, UploadService uploadService) {
        if (StringUtils.isNotBlank(testCase.getIssueId())
                && testCase.getSteps() != null && !testCase.getSteps().isEmpty()) {
            List<Map<String, String>> toReturn = new ArrayList<>();
            Map<String, String> temp = null;
            for (Steps steps : testCase.getSteps()) {
                temp = new HashMap<>();
                temp.put("step", steps.getStep() != null ? steps.getStep() : "");
                temp.put("data", steps.getTestData() != null ? steps.getTestData() : "");
                temp.put("result", steps.getResult() != null ? steps.getResult() : "");
                toReturn.add(temp);
            }
            return toReturn;
        }
        return null;
    }

    private static Object prepareDataFromAllowed(List<AllowedValue> allowedValues, Object value) {
        AllowedValue foundAllowedValue = allowedValues.stream()
                .filter(allowedValue -> allowedValue.compareToObject(value))
                .findFirst().get();
        return foundAllowedValue;
    }

    //create a map that stores testcase details
    private static Map<String, Map<String, Object>> creatTestCaseBody(TestCase testCase, UploadService uploadService) {
        Map<String, Object> issue = new HashMap<>();
        issue.put("project", uploadService.getProject());
        issue.put("issuetype", uploadService.getIssueType());

        //get all the mapping provided
        Object localData = null;
        Map<String, Object> temp = null;
        for (Map.Entry<Fields, String> entry : uploadService.getFieldStringMap().entrySet()) {
            if (!isStepField(entry.getKey())) {
                temp = testCase.findMapForCol(entry.getValue()); //see if we have data populated for this field.
                if (temp != null && StringUtils.isNotBlank((String) temp.get(entry.getValue()))) {
                    localData = prepareDataForField(entry.getKey(), (String) temp.get(entry.getValue()));
                    if (localData != null) {
                        issue.put(entry.getKey().getKey(), localData);
                    }
                }
            }
        }
        /* see https://developer.atlassian.com/jiradev/api-reference/jira-rest-apis/jira-rest-api-tutorials/jira-rest-api-example-create-issue */
        Map<String, Map<String, Object>> toReturn = new HashMap<>();
        toReturn.put("fields", issue);
        return toReturn;
    }

    //we use this function to upload a testcase.
    public static ResponseEntity uploadTestCase(TestCase testcase, UploadService uploadService) {
        ResponseEntity<IssueResponse> responseEntity = null;
        Map<String, Map<String, Object>> uploadData = creatTestCaseBody(testcase, uploadService);
        String url = StringUtils.join(
                uploadService.getBaseUrl(),
                uploadService.getBaseJiraURL(),
                "issue/");
        String jsonData = convertMapToJsonString(uploadData);
        responseEntity = uploadToJira(url, jsonData, IssueResponse.class, uploadService.getHttpHeaders());
        IssueResponse localIssue = responseEntity.getBody();
        if (CommonsFunctions.isResponseOk(responseEntity)) {
            if (localIssue != null) {
                testcase.setIssueId(localIssue.getId());
            }
            //take care of comments if any...
            /*if(StringUtils.isNotBlank(testcase.getComments())){
                url = StringUtils.join(url, localIssue.id(), "/comment");
                Map<String,String>temp = new HashMap<>();
                temp.put("body", testcase.getComments());
                jsonData = convertMapToJsonString(temp);
                responseEntity = uploadToJira(url,jsonData,LocalIssue.class,uploadService.getHttpHeaders());
            }*/
        }
        return responseEntity;
    }

    //we call this method to upload all our test steps for a testcase
    public static int uploadTestSteps(TestCase testCase, UploadService uploadService) {
        int counter = 0;
        if (StringUtils.isNotBlank(testCase.getIssueId())) {
            List<Map<String, String>> uploadSteps = creatTestStepsBody(testCase, uploadService);
            for (Map<String, String> step : uploadSteps) {
                if (CommonsFunctions.isResponseOk(uploadStep(step, testCase, uploadService))) {
                    counter++;
                }
            }
        }
        return counter;
    }


    public static ResponseEntity uploadTestCaseComment(String issueId, String comments) {
        ResponseEntity responseEntity = null;


        return null;
    }

    //we use this method as a static upload function
    public static <T> ResponseEntity<T> uploadToJira(String url, String payload, Class returnClassType, HttpHeaders localhttpHeaders) {
        ResponseEntity responseEntity = null;
        try {
            responseEntity =
                    CommonsFunctions.performRestCall(url,
                            payload,
                            HttpMethod.POST,
                            localhttpHeaders,
                            returnClassType
                    );

        } catch (RestClientException exception) {
            exception.printStackTrace();
        }
        return responseEntity;
    }


    //we use this function to upload a single step
    private static ResponseEntity uploadStep(Map<String, String> map, TestCase testCase, UploadService uploadService) {
        ResponseEntity responseEntity = null;
        String jsonData = convertMapToJsonString(map);
        String url = StringUtils.join(
                uploadService.getBaseUrl(),
                uploadService.getBaseZephyUrl(),
                "teststep/", testCase.getIssueId());
        //logger.info(url);
        //logger.info(jsonData);
        HttpHeaders testStepHeaders = new HttpHeaders();
        testStepHeaders.putAll(uploadService.getHttpHeaders());
        testStepHeaders.add("User-Agent", "ZFJWebUploader");
        testStepHeaders.add("AO_7DEABF", UUID.randomUUID().toString());
        testStepHeaders.add("AO_7DEABF", UUID.randomUUID().toString());


        responseEntity = uploadToJira(url, jsonData, String.class, testStepHeaders);
        return responseEntity;
    }


}
