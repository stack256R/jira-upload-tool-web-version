package com.jiraupload.util;


import com.vaadin.server.Sizeable;
import com.vaadin.ui.TextField;

/**
 * Created by robert.j.ssemmanda on 09/09/2017.
 */
public class UIElementsUtils {
    public static void beautifyTextField(TextField textField){
        if(textField != null){
            textField.setRequiredIndicatorVisible(true);
            textField.setWidth(50.0f, Sizeable.Unit.PERCENTAGE);
        }
    }

}
