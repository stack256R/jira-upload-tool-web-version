package com.jiraupload.util;

/**
 * Created by robert.j.ssemmanda on 05/09/2017.
 */
public enum Discriminator {
    BY_EMPTY_ROW,
    BY_SHEET,
    BY_ID_CHANGE,
    BY_TESTCASE_NAME_CHANGE
}
