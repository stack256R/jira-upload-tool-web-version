package com.jiraupload.app;

import com.jiraupload.views.MainView;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.spring.annotation.VaadinSessionScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import java.util.logging.Logger;

/**
 * Created by robert.j.ssemmanda on 31/08/2017.
 * This class will control the main UI for the app
 */
@Theme("mytheme")
@SpringUI
@SpringViewDisplay
//@VaadinSessionScope
public class AppUI extends UI implements ViewDisplay {

    //we use this fella to to hold our spring view(s)
    private Panel springViewDisplay;
    public final static Logger logger = Logger.getLogger(AppUI.class.getName());

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        //by doing so here we set the height to 100%  before adding any components
        setSizeFull();

        //create root layout for the page.
        final VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        setContent(root);

        springViewDisplay = new Panel();
        springViewDisplay.setResponsive(true);
        springViewDisplay.setSizeFull(); //we make it full size to cover the whole page before we add any components
        springViewDisplay.setStyleName(ValoTheme.PANEL_BORDERLESS);


        //add components to the root layout
        root.addComponents(springViewDisplay);
        //root.setExpandRatio(springViewDisplay, 20.0f);
    }


    @Override
    public void showView(View view) {
        springViewDisplay.setContent((Component) view);
    }
}
