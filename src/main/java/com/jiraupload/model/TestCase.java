package com.jiraupload.model;

import java.util.*;

/**
 * Created by robert.j.ssemmanda on 05/09/2017.
 */
public class TestCase {
    //store the rows that contain information about the testcase.. column and value
    List<Map<String, Object>> data;

    String defaultName;
    String issueId;
    UUID tracker;
    List<Steps> steps;

    public TestCase() {
        //data = new ArrayList<>();
        tracker = UUID.randomUUID();
    }

    /*
    * Utility functions for this class
    * */
    public void addIssueData(String column, Object value) {
        Map<String, Object> temp = findMapForCol(column);
        if (temp == null) {
            temp = new HashMap<>();
            temp.put(column, value);
            if (data == null) {
                data = new ArrayList<>();
            }
            data.add(temp);
        } else {
            temp.put(column, value);
        }
    }

    public void addSteps(Steps localsteps) {
        if (localsteps != null
            //&& StringUtils.isNotBlank(localsteps.getStep())
                ) { //make sure we check that step is not null and has a name value
            if (steps == null) {
                steps = new ArrayList<>();
            } else if (steps.contains(localsteps)) {
                return;
            }
            steps.add(localsteps);
        }
    }

    public Map<String, Object> findMapForCol(String col) {
        if (data != null) {
            for (Map<String, Object> temp : data) {
                if (temp.containsKey(col)) {
                    return temp;
                }
            }
        }
        return null;
    }

    public Object getFirstValueForColumn(String col) {
        for (Map<String, Object> temp : data) {
            if (temp.containsKey(col)) {
                return temp.get(col);
            }
        }
        return null;
    }

    /*
    * Getter and setter methods for the class
    * */

    public List<Map<String, Object>> getData() {
        return data;
    }

    public String getDefaultName() {
        return defaultName;
    }

    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    public List<Steps> getSteps() {
        return steps;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public UUID getTracker() {
        return tracker;
    }
}
