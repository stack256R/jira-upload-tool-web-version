package com.jiraupload.model;

/**
 * Created by robert.j.ssemmanda on 05/09/2017.
 */

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "required",
        "schema",
        "name",
        "key",
        "hasDefaultValue",
        "operations",
        "allowedValues"
})

public class Fields implements Serializable {

    private final static long serialVersionUID = -3820603163128857737L;
    @JsonProperty("required")
    private boolean required;
    @JsonProperty("schema")
    private Schema schema;
    @JsonProperty("name")
    private String name;
    @JsonProperty("key")
    private String key;
    @JsonProperty("hasDefaultValue")
    private boolean hasDefaultValue;
    @JsonProperty("operations")
    private List<Object> operations = null;
    @JsonProperty("allowedValues")
    private List<AllowedValue> allowedValues = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("required")
    public boolean isRequired() {
        return required;
    }

    @JsonProperty("required")
    public void setRequired(boolean required) {
        this.required = required;
    }

    @JsonProperty("schema")
    public Schema getSchema() {
        return schema;
    }

    @JsonProperty("schema")
    public void setSchema(Schema schema) {
        this.schema = schema;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("hasDefaultValue")
    public boolean isHasDefaultValue() {
        return hasDefaultValue;
    }

    @JsonProperty("hasDefaultValue")
    public void setHasDefaultValue(boolean hasDefaultValue) {
        this.hasDefaultValue = hasDefaultValue;
    }

    @JsonProperty("operations")
    public List<Object> getOperations() {
        return operations;
    }

    @JsonProperty("operations")
    public void setOperations(List<Object> operations) {
        this.operations = operations;
    }

    @JsonProperty("allowedValues")
    public List<AllowedValue> getAllowedValues() {
        return allowedValues;
    }

    @JsonProperty("allowedValues")
    public void setAllowedValues(List<AllowedValue> allowedValues) {
        this.allowedValues = allowedValues;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(required).append(schema).append(name).append(key).append(hasDefaultValue).append(operations).append(allowedValues).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Fields) == false) {
            return false;
        }
        Fields rhs = ((Fields) other);
        return new EqualsBuilder().append(required, rhs.required).append(schema, rhs.schema).append(name, rhs.name).append(key, rhs.key).append(hasDefaultValue, rhs.hasDefaultValue).append(operations, rhs.operations).append(allowedValues, rhs.allowedValues).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
