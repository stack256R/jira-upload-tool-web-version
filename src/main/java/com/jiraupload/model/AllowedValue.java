package com.jiraupload.model;

/**
 * Created by robert.j.ssemmanda on 05/09/2017.
 */

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "self",
        "id",
        "description",
        "iconUrl",
        "name",
        "subtask",
        "avatarId"
})

public class AllowedValue implements Serializable {

    private final static long serialVersionUID = -6479241587998400208L;
    @JsonProperty("self")
    private String self;
    @JsonProperty("id")
    private String id;
    @JsonProperty("description")
    private String description;
    @JsonProperty("iconUrl")
    private String iconUrl;
    @JsonProperty("name")
    private String name;
    @JsonProperty("subtask")
    private boolean subtask;
    @JsonProperty("avatarId")
    private int avatarId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("self")
    public String getSelf() {
        return self;
    }

    @JsonProperty("self")
    public void setSelf(String self) {
        this.self = self;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("iconUrl")
    public String getIconUrl() {
        return iconUrl;
    }

    @JsonProperty("iconUrl")
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("subtask")
    public boolean isSubtask() {
        return subtask;
    }

    @JsonProperty("subtask")
    public void setSubtask(boolean subtask) {
        this.subtask = subtask;
    }

    @JsonProperty("avatarId")
    public int getAvatarId() {
        return avatarId;
    }

    @JsonProperty("avatarId")
    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(self).append(id).append(description).append(iconUrl).append(name).append(subtask).append(avatarId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof AllowedValue)) {
            return false;
        }
        AllowedValue rhs = ((AllowedValue) other);
        return new EqualsBuilder().append(self, rhs.self).append(id, rhs.id).append(description, rhs.description).append(iconUrl, rhs.iconUrl).append(name, rhs.name).append(subtask, rhs.subtask).append(avatarId, rhs.avatarId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

    private boolean compareToMapObject(Map<String, String> map) {
        if (map != null && !map.isEmpty() && map.values().size() < 2) {
            if (map.containsKey("id") && StringUtils.isNotBlank(this.getId())) {
                return this.getId().equalsIgnoreCase(map.get("id"));
            } else if (map.containsKey("name") && StringUtils.isNotBlank(this.getName())) {
                return this.getName().equalsIgnoreCase(map.get("name"));
            }
        }
        return false;
    }

    private boolean compareToString(String compareString) {
        return StringUtils.isNotBlank(compareString) && (compareString.equalsIgnoreCase(this.getId()) || compareString.equalsIgnoreCase(this.getName()));
    }

    public boolean compareToObject(Object object) {
        if (object.getClass().isInstance(Map.class)) {
            return compareToMapObject(Map.class.cast(object));
        } else if (object.getClass().isInstance(String.class)) {
            return compareToString(String.class.cast(object));
        }
        return false;
    }

}
