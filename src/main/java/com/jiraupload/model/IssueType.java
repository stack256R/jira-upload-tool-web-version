package com.jiraupload.model;

/**
 * Created by robert.j.ssemmanda on 05/09/2017.
 */

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "self",
        "id",
        "description",
        "iconUrl",
        "name",
        "subtask",
        "expand",
        "fields"
})
public class IssueType implements Serializable {

    private final static long serialVersionUID = -8355013093316231532L;
    @JsonProperty("self")
    private String self;
    @JsonProperty("id")
    private String id;
    @JsonProperty("description")
    private String description;
    @JsonProperty("iconUrl")
    private String iconUrl;
    @JsonProperty("name")
    private String name;
    @JsonProperty("subtask")
    private boolean subtask;
    @JsonProperty("expand")
    private String expand;
    @JsonProperty("fields")
    private Map<String, Fields> fields;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("self")
    public String getSelf() {
        return self;
    }

    @JsonProperty("self")
    public void setSelf(String self) {
        this.self = self;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("iconUrl")
    public String getIconUrl() {
        return iconUrl;
    }

    @JsonProperty("iconUrl")
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("subtask")
    public boolean isSubtask() {
        return subtask;
    }

    @JsonProperty("subtask")
    public void setSubtask(boolean subtask) {
        this.subtask = subtask;
    }

    @JsonProperty("expand")
    public String getExpand() {
        return expand;
    }

    @JsonProperty("expand")
    public void setExpand(String expand) {
        this.expand = expand;
    }

    @JsonProperty("fields")
    public Map<String, Fields> getFields() {
        return fields;
    }

    @JsonProperty("fields")
    public void setFields(Map<String, Fields> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(self).append(id).append(description).append(iconUrl).append(name).append(subtask).append(expand).append(fields).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IssueType) == false) {
            return false;
        }
        IssueType rhs = ((IssueType) other);
        return new EqualsBuilder().append(self, rhs.self).append(id, rhs.id).append(description, rhs.description).append(iconUrl, rhs.iconUrl).append(name, rhs.name).append(subtask, rhs.subtask).append(expand, rhs.expand).append(fields, rhs.fields).append(additionalProperties, rhs.additionalProperties).isEquals();
    }
}
