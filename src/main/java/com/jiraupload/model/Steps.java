package com.jiraupload.model;

/**
 * Created by robert.j.ssemmanda on 05/09/2017.
 */
public class Steps {

    private String step;

    private String result;

    private String testData;


    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTestData() {
        return testData;
    }

    public void setTestData(String testData) {
        this.testData = testData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Steps)) return false;

        Steps steps = (Steps) o;

        if (!getStep().equals(steps.getStep())) return false;
        if (getResult() != null ? !getResult().equals(steps.getResult()) : steps.getResult() != null) return false;
        return getTestData() != null ? getTestData().equals(steps.getTestData()) : steps.getTestData() == null;
    }

    @Override
    public int hashCode() {
        int result1 = getStep().hashCode();
        result1 = 31 * result1 + (getResult() != null ? getResult().hashCode() : 0);
        result1 = 31 * result1 + (getTestData() != null ? getTestData().hashCode() : 0);
        return result1;
    }
}
