package com.jiraupload.panels;

import com.jiraupload.factory.UIElementsFactory;
import com.jiraupload.util.BroadCaster;
import com.jiraupload.util.UIElementsUtils;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by robert.j.ssemmanda on 09/09/2017.
 * This class will contain the setup information for the settings
 * view and Tab
 */
@Component
@Scope("prototype")
public class SettingsPanel extends VerticalLayout implements BroadCaster.BroadcastListener {

    //we use this function to intialize our  view... and setup all that is needed.
    private TextField jiraUrl, usersName, password;
    private Button loginButton;
    private static int MAX_FIELD_CHAR_COUNT = 1000;

    @Autowired
    DragDropPanel dragDropPanel;


    @PostConstruct
    private void init(){
        setSizeFull();
        //create our tabs for the user input and also the drag and drop panel.

        TabSheet settingTabs = UIElementsFactory.createTabSheet(null);
        settingTabs.setSizeFull();

        addComponentsAndExpand(
                UIElementsFactory.createLabel("Connection Settings ",null),
                settingTabs
        );

        settingTabs.addTab(create1stTab(),"Manual");
        settingTabs.addTab(create2ndTab(),"Config file");

    }

    /*
    * We use this method to setup all the fields in this layout / panel and an also
    * to give styling etc
    * */
    private void setUpFields(){
        jiraUrl = UIElementsFactory.createTextField("JIRA URL : ",null,null,MAX_FIELD_CHAR_COUNT,false);
        usersName = UIElementsFactory.createTextField("USER ID : ",null,null,MAX_FIELD_CHAR_COUNT,false);
        password = UIElementsFactory.createTextField("PASSWORD : ",null,null,MAX_FIELD_CHAR_COUNT,false);
        loginButton = UIElementsFactory.createButton("CONNECT",null,null);

        UIElementsUtils.beautifyTextField(jiraUrl);
        UIElementsUtils.beautifyTextField(usersName);
        UIElementsUtils.beautifyTextField(password);

        jiraUrl.setIcon(VaadinIcons.LINK);
        usersName.setIcon(VaadinIcons.USER_CARD);
        password.setIcon(VaadinIcons.PASSWORD);

        loginButton.addStyleName(ValoTheme.BUTTON_FRIENDLY);

    }

    private FormLayout create1stTab(){
        setUpFields();
        FormLayout formLayout = UIElementsFactory.createFormLayout(null);
        formLayout.setSizeFull();
        formLayout.addComponents(jiraUrl,usersName,password,loginButton);
        return formLayout;
    }

    private VerticalLayout create2ndTab(){
        return dragDropPanel;
        //return UIElementsFactory.createFormLayout(null);
    }

    /*
    * utility functions*/

    //a file has completed uploading
    @Override
    public void receiveBroadcast(int type) {
        if(type == 0){ //config file

        }else{ //excel file

        }
    }

    public void setLoginButtonListener(Button.ClickListener clickListener){
        if(loginButton != null){
            loginButton.addClickListener(clickListener);
        }
    }

    public TextField getJiraUrl() {
        return jiraUrl;
    }

    public TextField getUsersName() {
        return usersName;
    }

    public TextField getPassword() {
        return password;
    }

}
