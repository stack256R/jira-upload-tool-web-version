package com.jiraupload.panels;

import com.jiraupload.factory.UIElementsFactory;
import com.jiraupload.util.BroadCaster;
import com.vaadin.server.StreamVariable;
import com.vaadin.ui.*;
import com.vaadin.ui.dnd.FileDropTarget;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by robert.j.ssemmanda on 16/09/2017.
 */
@Component
@Scope("prototype")
public class DragDropPanel extends VerticalLayout {

    public static Logger logger = Logger.getLogger(DragDropPanel.class.getName());
    private final Label dragLabel = UIElementsFactory.createLabel("Drag and Drop your file here.", null);
    private final float COMP_HEIGHT = 150f;
    private final int FILE_SIZE_LIMIT = 20 * 1024 * 1024;
    private final Panel fileUploadPanel = UIElementsFactory.createPanel(null);
    private ProgressBar progressBar = new ProgressBar();


    private String uploadFileName;
    private String fileExtension;
    private ByteArrayOutputStream byteArrayOutputStream = null;

    //we use this function to setup the view
    @PostConstruct
    private void init(){
        //setSizeFull();

        dragLabel.setWidth(100, Unit.PERCENTAGE);
        dragLabel.addStyleName("v-label-customDragDrobLabel");

        //setup the upload panel
        VerticalLayout verticalLayout = UIElementsFactory.creatVerticalLayout(null);
        verticalLayout.setHeight(COMP_HEIGHT, Unit.PIXELS);
        verticalLayout.setWidth(100, Unit.PERCENTAGE);
        verticalLayout.addComponent(dragLabel);
        verticalLayout.setComponentAlignment(dragLabel, Alignment.MIDDLE_CENTER);


        //style Progress Bar
        progressBar.setIndeterminate(true);
        progressBar.setVisible(false);
        verticalLayout.addComponent(progressBar);
        verticalLayout.setComponentAlignment(progressBar, Alignment.MIDDLE_CENTER);

        fileUploadPanel.setContent(verticalLayout);
        fileUploadPanel.addStyleName("customDragDropPanel");

        new FileDropTarget<>(verticalLayout,fileDropEvent -> {
            fileDropEvent.getFiles().forEach(file -> {
                // Max 1 MB files are uploaded
                if (file.getFileSize() <= FILE_SIZE_LIMIT) {
                    final ByteArrayOutputStream bas = new ByteArrayOutputStream();
                    file.setStreamVariable(new StreamVariable() {
                        @Override
                        public OutputStream getOutputStream() {
                            return bas;
                        }

                        @Override
                        public boolean listenProgress() {
                            return false;
                        }

                        @Override
                        public void onProgress(StreamingProgressEvent streamingProgressEvent) {

                        }

                        @Override
                        public void streamingStarted(StreamingStartEvent streamingStartEvent) {

                        }

                        @Override
                        public void streamingFinished(StreamingEndEvent streamingEndEvent) {
                            progressBar.setVisible(false);
                            byteArrayOutputStream = bas;
                            uploadFileName = file.getFileName();
                            fileExtension = file.getType();
                            //determine the file type
                            List<String> excelTypes = Arrays.asList("application/vnd.ms-excel.addin.macroEnabled.12", "application/vnd.ms-excel.sheet.macroEnabled.12",
                                    "application/vnd.ms-excel", "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
                                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
                            BroadCaster.broadcast(excelTypes.contains(file.getType())?1:0);
                            //broadcast a message that the file has finished uploading.
                        }

                        @Override
                        public void streamingFailed(StreamingErrorEvent streamingErrorEvent) {
                            progressBar.setVisible(false);
                        }

                        @Override
                        public boolean isInterrupted() {
                            return false;
                        }
                    });
                    progressBar.setVisible(true);
                }else{
                    Notification.show(
                            "File rejected. Max "+FILE_SIZE_LIMIT+"MB files are accepted by Sampler",
                            Notification.Type.WARNING_MESSAGE);
                }
            });
        });

        //add components to layout and expand
        addComponentsAndExpand(fileUploadPanel);

    }

    //getter and setter methods
    public String getUploadFileName() {
        return uploadFileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public ByteArrayOutputStream getByteArrayOutputStream() {
        return byteArrayOutputStream;
    }
}
