package com.jiraupload;


import com.jiraupload.model.*;
import com.jiraupload.services.UploadService;
import com.jiraupload.util.Discriminator;
import com.jiraupload.util.LoadExcelData;
import com.jiraupload.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@SpringBootApplication
public class UploadappApplication {

    private static final Logger logger = Logger.getLogger(UploadappApplication.class.getName());

    static String defJiraURL = "https://devstack256.atlassian.net";
    static String Jirausername = "robert.j.ssemmanda@accenture.com";
    static String JiraPassword = "lazarusdev";

    @Autowired
    UploadService uploadService;

    @Value("classpath:Template.xlsx")
    private Resource res;

	public static void main(String[] args) {
		SpringApplication.run(UploadappApplication.class, args);
	}

	private void testExecutionofApp(){
        uploadService.setBaseUrl(defJiraURL);
        uploadService.setUserName(Jirausername);
        uploadService.setPassword(JiraPassword.toCharArray());

        logger.info("====== Starting User Actions ======= ");
        uploadService.isLoginSuccessful();
        Project project =  uploadService.retrieveProjects().get(0);//.forEach(project -> logger.info(project.getName()));
        //logger.info(project.getIssuetypes().toString());
        uploadService.setProject(project);
        MetaModel metaModel = uploadService.retrieveMeta();
        logger.info(metaModel.getProjects().get(0).getName());
        IssueType issueType = null;
        for (IssueType issue : metaModel.getProjects().get(0).getIssuetypes()){
            if (issue.getName().equalsIgnoreCase("Test")){
                issueType = issue;
                break;
            }
        }
        uploadService.setIssueTypeAndLoadFields(issueType);
        for(Fields fields : uploadService.getFieldStringMap().keySet()){
            if(fields.getName().equalsIgnoreCase("summary")){
                uploadService.getFieldStringMap().put(fields,"A");
            }
            if(fields.getName().equalsIgnoreCase("Description")){
                uploadService.getFieldStringMap().put(fields,"L");
            }
            if(fields.getName().equalsIgnoreCase("labels")){
                uploadService.getFieldStringMap().put(fields,"F");
            }
            if(fields.getName().equalsIgnoreCase(UploadService.TEST_STEP_NAME)){
                uploadService.getFieldStringMap().put(fields,"B");
            }

            if(fields.getName().equalsIgnoreCase(UploadService.TEST_STEP_DATA)){
                uploadService.getFieldStringMap().put(fields,"C");
            }

            if(fields.getName().equalsIgnoreCase(UploadService.TEST_STEP_RESULTS)){
                uploadService.getFieldStringMap().put(fields,"D");
            }
        }
        //List<String> columns = uploadService.extractColumnList();
        Map<Fields,String> mapedCols = uploadService.cleanUpFieldMaping();
        //columns.forEach(k-> logger.info("==>"+k));
        List<TestCase> testCaseList = null;
        try {
            testCaseList = LoadExcelData.loadIssuesFromExcelFile(
                    res.getInputStream(), Discriminator.BY_TESTCASE_NAME_CHANGE,
                    mapedCols,null,
                    true,2,
                    "A", "A");
        } catch (IOException e) {
            logger.info(e.getLocalizedMessage());
        }
        /*Map<Fields,String> tempMap = uploadService.cleanUpFieldMaping();
        tempMap.forEach((k,v)->{
            logger.info("key : "+k.getName()+" value : "+ v);
        });*/
        testCaseList.forEach(testCase -> {
            logger.info("defaultName : "+testCase.getDefaultName()+" Description : "+ testCase.getFirstValueForColumn("L"));
            if(testCase.getSteps() != null){
                testCase.getSteps().forEach(steps -> {
                    logger.info("step name : "+steps.getStep()+" step data : "+steps.getTestData() + " step result : "+steps.getResult());
                });
            }

            UploadUtil.uploadTestCase(testCase,uploadService);
            UploadUtil.uploadTestSteps(testCase,uploadService);
        });

        logger.info("====== End User Actions ======= ");
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx){
	    return args -> {
	        //testExecutionofApp();
        };
	}
}
