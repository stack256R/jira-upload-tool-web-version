package com.jiraupload.services;

import com.jiraupload.model.*;
import com.jiraupload.util.CommonsFunctions;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import javax.annotation.PostConstruct;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.jiraupload.util.CommonsFunctions.convertMapToJsonString;
import static com.jiraupload.util.CommonsFunctions.performRestCall;

/**
 * Created by robert.j.ssemmanda on 31/08/2017.
 * This class acts as a service for all the upload functionality we will need
 * to upload the file to jira
 */
@Service
@Scope("prototype")
//@VaadinSessionScope
public class UploadService {

    private static final Logger logger = Logger.getLogger(UploadService.class.getName());

    /*
    * Default Jira and Zephyr Urls to interact with the Jira API.
    * */
    final private String baseJiraURL = "/rest/api/latest/";
    final private String baseZephyUrl = "/rest/zapi/latest/";
    final private String testStepsUrl = "/teststeps/";
    final public static String TEST_STEP_NAME = "Test Step";
    final public static String TEST_STEP_DATA = "Test Data";
    final public static String TEST_STEP_RESULTS = "Expected Result";

    /*
    * Variable declarations
    * */

    //to store the base url
    private String baseUrl;

    //to store the username
    private String userName;

    //selected Project
    private Project project;

    //selected Issue Type
    private IssueType issueType;

    //store the password --> we use a character array so as it is safer.
    private char[] password;

    //we use this variable to save or cookie from the server and preserve our session
    private HttpHeaders httpHeaders;

    //we keep a copy of our meta model for reference to all project information
    private MetaModel metaModel;

    //we reference our common functions class for all the common functions
    @Autowired
    private CommonsFunctions commonsFunctions;

    //we use this variable to store our fieldmaps and value
    private Map<Fields, String> fieldStringMap;


    /*
    * We call this method to setup a few things after
    * the constructor
    * */
    @PostConstruct
    private void init() {
        httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
    }

    /*
    * All the utility functions
    * */

    //we use this function to provide a list of avaliable issuetypes
    public List<IssueType> getAvailableIssueTypes() {
        if (metaModel != null) {
            //we know that only one project will be returned
            return metaModel.getProjects() == null ||
                    metaModel.getProjects().isEmpty() ? null : metaModel.getProjects().get(0).getIssuetypes();
        }
        return null;
    }

    //we use this method to set a selected issueType
    public void setIssueTypeAndLoadFields(IssueType selIssueType) {
        issueType = selIssueType;
        if (fieldStringMap == null) {
            fieldStringMap = new HashMap<>();
        }
        fieldStringMap.clear();
        selIssueType.getFields().forEach((fieldName, field) -> {
            if (!(fieldName.equals("issuetype") || fieldName.equals("project"))) { //we remove the issuetype and project as we will not need it later
                fieldStringMap.put(field, "");
            }
        });
        //add the extra fields for Test Step, Test Data, Expected Results
        fieldStringMap.put(createField(TEST_STEP_NAME), "");
        fieldStringMap.put(createField(TEST_STEP_DATA), "");
        fieldStringMap.put(createField(TEST_STEP_RESULTS), "");
    }

    //use this function to create a new field.
    private Fields createField(String name) {
        Fields fields = new Fields();
        fields.setKey(name);
        fields.setName(name);
        fields.setRequired(false);
        fields.setHasDefaultValue(false);
        return fields;
    }

    //we use this method to login the user and acquire the cookie if any
    public boolean isLoginSuccessful() {
        String json = commonsFunctions.createUserJsonMap(userName, password);
        try {
            ResponseEntity responseEntity;
            responseEntity = performRestCall(
                    StringUtils.join(baseUrl, "/rest/auth/1/session"),
                    json,
                    HttpMethod.POST,
                    httpHeaders,
                    String.class
            );
            //extract the header for the cookie from the response.
            CommonsFunctions.retrieveAndSaveCookie(responseEntity.getHeaders(), httpHeaders);
            return true;
        } catch (RestClientException exception) {
            exception.printStackTrace();
        }
        return false;
    }

    //function that retrieves projects / projects related information
    private <T> List<T> retrieveDetailsFromJira(String url, Class responsType, HttpMethod httpMethod) {
        List<T> toReturn = null;
        try {
            ResponseEntity<T[]> responseEntity =
                    CommonsFunctions.performRestCall(url,
                            null,
                            httpMethod,
                            httpHeaders,
                            responsType
                    );
            toReturn = Arrays.asList(responseEntity.getBody()); //check for status in the responses
        } catch (RestClientException exception) {
            exception.printStackTrace();
        }
        return toReturn;
    }

    //function to retrieve single object data
    private <T> T retrieveDetailFromJira(String url, Class responsType, HttpMethod httpMethod) {
        T toReturn = null;
        try {
            ResponseEntity<T> responseEntity =
                    CommonsFunctions.performRestCall(url,
                            null,
                            httpMethod,
                            httpHeaders,
                            responsType
                    );
            toReturn = responseEntity.getBody(); //check for status in the responses
        } catch (RestClientException exception) {
            exception.printStackTrace();
        }
        return toReturn;
    }

    //function we use to retrieve all the projects available for this user
    public List<Project> retrieveProjects() {
        List<Project> toReturn = retrieveDetailsFromJira(
                StringUtils.join(baseUrl, baseJiraURL, "project"),
                Project[].class,
                HttpMethod.GET);
        return toReturn;
    }

    /*
    * function to retrieve the meta data for a given project --> this is also its the available fields
    * that are available from Jira for this given project
    * */
    public MetaModel retrieveMeta() {
        MetaModel toReturn = retrieveDetailFromJira(StringUtils.join(
                baseUrl,
                baseJiraURL,
                "issue/createmeta?projectIds=",
                project.getId(),
                "&expand=projects.issuetypes.fields"),
                MetaModel.class,
                HttpMethod.GET);
        return toReturn;
    }

    //alas we use this method here to prepare data for upload
    public Map<Fields, String> cleanUpFieldMaping() {
        Map<Fields, String> temp = fieldStringMap
                .entrySet()
                .stream()
                .filter(map -> StringUtils.isNotBlank(map.getValue()))
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
        return temp;
    }

    public List<String> extractColumnList() {
        List<String> toReturn = null;
        Map<Fields, String> temp = cleanUpFieldMaping();
        if (temp.values() != null) {
            toReturn = new ArrayList<>();
            toReturn.addAll(temp.values());
        }
        return toReturn;
    }

    //function to logout
    public void logOutUser() {
//        loggedInSuccess = false;
    }

    /*
    * All the getter and setter functionts
    * */

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public IssueType getIssueType() {
        return issueType;
    }

    public HttpHeaders getHttpHeaders() {
        return httpHeaders;
    }

    public MetaModel getMetaModel() {
        return metaModel;
    }

    public void setMetaModel(MetaModel metaModel) {
        this.metaModel = metaModel;
    }

    public Map<Fields, String> getFieldStringMap() {
        return fieldStringMap;
    }


    public String getBaseZephyUrl() {
        return baseZephyUrl;
    }

    public String getTestStepsUrl() {
        return testStepsUrl;
    }

    public String getBaseJiraURL() {
        return baseJiraURL;
    }
}
